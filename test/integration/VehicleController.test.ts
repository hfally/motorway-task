import request from "supertest";
import sinon, { SinonSandbox } from "sinon";
import { bootstrap, BootstrapSetting } from "../utils/bootstrap";
import { dropDatabaseConnection } from "../utils/database";
import { DatabaseSeeder } from "../utils/seeders/DatabaseSeeder";
import { Container } from "typedi";
import { VehicleService } from "../../src/services/VehicleService";
import { LogService } from "../../src/services/LogService";

describe('Vehicle Controller', () => {
    let settings: BootstrapSetting;
    let sandbox: SinonSandbox;

    beforeAll(async () => {
        settings = await bootstrap();
    });

    afterAll(async () => {
        await dropDatabaseConnection(settings);
    });

    beforeEach(async () => {
        // seed DB.
        await DatabaseSeeder.seed();
        sandbox = sinon.createSandbox();

        sandbox.stub(Container.get(LogService), 'error');
    });

    afterEach(async () => {
        sandbox.restore();
    })

    describe('GET /vehicles/{vehicleId}/state', () => {
        it('should return 404 if vehicle is not found', async () => {
            const { status, body } = await request(settings.app).get('/vehicles/20/state');

            expect(status).toEqual(404);
            expect(body).toHaveProperty('message');
            expect(body.message).toEqual('Vehicle not found');
        });

        test.each([
            {
                value: 'fake-timestamp',
                errorMessages: [
                    'timestamp must match YYYY-MM-DD, YYYY-MM-DD HH:MM:SS or YYYY-MM-DD HH:MM:SS+OFFSET',
                    'timestamp must be a valid ISO 8601 date string',
                ],
            },
            {
                value: '123',
                errorMessages: [
                    'timestamp must match YYYY-MM-DD, YYYY-MM-DD HH:MM:SS or YYYY-MM-DD HH:MM:SS+OFFSET',
                    'timestamp must be a valid ISO 8601 date string',
                ],
            },
            {
                value: '1234',
                errorMessages: [ 'timestamp must match YYYY-MM-DD, YYYY-MM-DD HH:MM:SS or YYYY-MM-DD HH:MM:SS+OFFSET' ],
            },
            {
                value: '2009-02',
                errorMessages: [ 'timestamp must match YYYY-MM-DD, YYYY-MM-DD HH:MM:SS or YYYY-MM-DD HH:MM:SS+OFFSET' ],
            },
            {
                value: '2009-14-01 12:34',
                errorMessages: [ 'timestamp must be a valid ISO 8601 date string' ],
            },
        ])('should return 400 if timestamp is not formatted well: $value', async ({ value, errorMessages }) => {
            const { status, body } = await request(settings.app).get(`/vehicles/2/state?timestamp=${value}`);

            expect(status).toEqual(400);
            expect(body).toHaveProperty('message');
            expect(body).toHaveProperty('errors');
            expect(body.errors).toHaveProperty('timestamp');
            expect(body.message).toEqual("Invalid queries, check 'errors' property for more info.");
            expect(body.errors.timestamp).toEqual(errorMessages);
        });

        it('should send a 500 if an unknown error is thrown', async () => {
            const vehicleService = Container.get(VehicleService);

            sandbox.stub(vehicleService, 'getState').rejects(new Error('An error occurred'));

            const { status, body } = await request(settings.app).get('/vehicles/3/state').query({
                timestamp: '2022-09-11 09:11:45+00',
            });

            expect(status).toEqual(500);
            expect(body).toHaveProperty('message');
            expect(body.message).toEqual('An error occurred');
        });

        it('should return current state if no timestamp is provided', async () => {
            const { status, body } = await request(settings.app).get('/vehicles/2/state');

            expect(status).toEqual(200);
            expect(body).toHaveProperty('message');
            expect(body).toHaveProperty('data');
            expect(body.message).toEqual('Vehicle state retrieved');
            expect(body.data).toEqual({
                make: 'AUDI',
                model: 'A4',
                state: 'selling',
                currentState: 'selling',
            });
        });

        it('should return state at timestamp if timestamp is provided', async () => {
            const { status, body } = await request(settings.app).get('/vehicles/3/state').query({
                timestamp: '2022-09-11 09:11:45+00',
            });

            expect(status).toEqual(200);
            expect(body).toHaveProperty('message');
            expect(body).toHaveProperty('data');
            expect(body.message).toEqual('Vehicle state retrieved');
            expect(body.data).toEqual({
                make: 'VW',
                model: 'GOLF',
                state: 'quoted',
                currentState: 'sold',
            });
        });
    });
});
