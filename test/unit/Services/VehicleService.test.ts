import { bootstrap, BootstrapSetting } from "../../utils/bootstrap";
import { dropDatabaseConnection } from "../../utils/database";
import { DatabaseSeeder } from "../../utils/seeders/DatabaseSeeder";
import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { Container } from "typedi";
import { VehicleService } from "../../../src/services/VehicleService";
import { CacheService } from "../../../src/services/CacheService";
import { VehicleRepository } from "../../../src/repositories/VehicleRepository";
import { BadRequestError, NotFoundError } from "routing-controllers";
import { refreshCacheMemory } from "../../utils/cache";

describe('Vehicle Service', () => {
    let setting: BootstrapSetting;
    let sandbox: SinonSandbox;
    let cacheService: CacheService;
    let vehicleService: VehicleService;
    let getCurrentStateSpy: SinonSpy;
    let getStateAtSpy: SinonSpy;
    let cacheServiceSetSpy: SinonSpy;
    let cacheServiceGetSpy: SinonSpy;
    let cacheServiceDeleteSpy: SinonSpy;
    let vehicleRepoFindSpy: SinonSpy;
    let vehicleRepoFindStateAtSpy: SinonSpy;

    beforeAll(async () => {
        setting = await bootstrap();
    });

    afterAll(async () => {
        await dropDatabaseConnection(setting);
    });

    beforeEach(async () => {
        await DatabaseSeeder.seed();
        sandbox = sinon.createSandbox();

        cacheService = Container.get(CacheService);
        cacheServiceSetSpy = sandbox.spy(cacheService, 'set');
        cacheServiceGetSpy = sandbox.spy(cacheService, 'get');
        cacheServiceDeleteSpy = sandbox.spy(cacheService, 'delete');

        vehicleService = Container.get(VehicleService);
        getCurrentStateSpy = sandbox.spy(vehicleService, 'getCurrentState');
        getStateAtSpy = sandbox.spy(vehicleService, 'getStateAt');

        const vehicleRepository = Container.get(VehicleRepository);
        vehicleRepoFindSpy = sandbox.spy(vehicleRepository, 'find');
        vehicleRepoFindStateAtSpy = sandbox.spy(vehicleRepository, 'findStateAt');
    });

    afterEach(() => {
        sandbox.restore();
        refreshCacheMemory();
    });

    describe('getState', () => {
        it('should get current state if timestamp is empty', async () => {
            const stateRecord = await vehicleService.getState(2);

            expect(stateRecord).toEqual({
                make: 'AUDI',
                model: 'A4',
                state: 'selling',
                currentState: 'selling',
            });
            expect(getCurrentStateSpy.calledOnce).toBeTruthy();
            expect(getStateAtSpy.calledOnce).toBeFalsy();
        });

        it('should get state at given timestamp if timestamp is provided', async () => {
            const stateRecord = await vehicleService.getState(2, '2022-09-11 09:11:45+00');

            expect(stateRecord).toEqual({
                make: 'AUDI',
                model: 'A4',
                state: 'quoted',
                currentState: 'selling',
            });
            expect(getStateAtSpy.calledOnce).toBeTruthy();
            expect(getCurrentStateSpy.calledOnce).toBeFalsy();
        });
    });

    describe('getCurrentState', () => {
        it('should throw vehicle-not-found if not found in the db and should cache for next request', async () => {
            const errors: any[] = [];

            for (let i = 1; i <= 2; i++) {
                try {
                    await vehicleService.getCurrentState(20);
                } catch (error) {
                    errors.push(error);
                }
            }

            expect(errors).toHaveLength(2);
            expect(errors[0]).toBeInstanceOf(NotFoundError);
            expect(errors[1]).toBeInstanceOf(NotFoundError);
            expect(errors[0].message).toEqual('Vehicle not found');
            expect(errors[1].message).toEqual('Vehicle not found');

            expect(vehicleRepoFindSpy.calledOnce).toBeTruthy();

            expect(cacheServiceSetSpy.calledOnceWith('vehicle-state:20:not-found', 'true', 86400)).toBeTruthy();
            expect(cacheServiceGetSpy.calledWith('vehicle-state:20:not-found')).toBeTruthy();
        });

        it('should return vehicle state from DB and cache record for next request', async () => {
            const responses: any[] = [];

            for (let i = 1; i <= 2; i++) {
                responses.push(await vehicleService.getCurrentState(2));
            }

            expect(responses).toHaveLength(2);
            expect(responses[0]).toEqual({
                make: 'AUDI',
                model: 'A4',
                state: 'selling',
                currentState: 'selling',
            });
            expect(responses[1]).toEqual({
                make: 'AUDI',
                model: 'A4',
                state: 'selling',
                currentState: 'selling',
            });

            expect(vehicleRepoFindSpy.calledOnce).toBeTruthy();

            expect(cacheServiceSetSpy.calledOnceWith(
                'vehicle-state:2',
                JSON.stringify({
                    id: 2,
                    make: 'AUDI',
                    model: 'A4',
                    currentState: 'selling',
                })
            )).toBeTruthy();
            expect(cacheServiceGetSpy.calledWith('vehicle-state:2')).toBeTruthy();
        });
    });

    describe('getStateAt', () => {
        it('should throw vehicle-not-found if vehicle id not found in DB and cache for next request', async () => {
            const errors: any[] = [];

            for (let i = 1; i <= 2; i++) {
                try {
                    await vehicleService.getStateAt(20, '2022-09-12 10:00:00+00');
                } catch (error) {
                    errors.push(error);
                }
            }

            expect(errors).toHaveLength(2);
            expect(errors[0]).toBeInstanceOf(NotFoundError);
            expect(errors[1]).toBeInstanceOf(NotFoundError);
            expect(errors[0].message).toEqual('Vehicle not found');
            expect(errors[1].message).toEqual('Vehicle not found');

            expect(vehicleRepoFindStateAtSpy.calledOnce).toBeTruthy();

            expect(cacheServiceSetSpy.calledOnceWith('vehicle-state:20:not-found', 'true', 86400)).toBeTruthy();
            expect(cacheServiceGetSpy.calledWith('vehicle-state:20:not-found')).toBeTruthy();
        });

        it('should throw timestamp-not-found if vehicle is found but not timestamp match is found in DB and, cache for  the next request', async () => {
            const errors: any[] = [];

            for (let i = 1; i <= 2; i++) {
                try {
                    await vehicleService.getStateAt(2, '2010-09-12 10:00:00+00');
                } catch (error) {
                    errors.push(error);
                }
            }

            expect(errors).toHaveLength(2);
            expect(errors[0]).toBeInstanceOf(BadRequestError);
            expect(errors[1]).toBeInstanceOf(BadRequestError);
            expect(errors[0].message).toEqual('No state found for vehicle at the given timestamp');
            expect(errors[1].message).toEqual('No state found for vehicle at the given timestamp');

            expect(vehicleRepoFindStateAtSpy.calledOnce).toBeTruthy();

            expect(cacheServiceSetSpy.calledOnceWith('vehicle-state:2:2010-09-12 10:00:00+00:not-found', 'true')).toBeTruthy();
            expect(cacheServiceGetSpy.calledWith('vehicle-state:2:2010-09-12 10:00:00+00:not-found')).toBeTruthy();
        });

        it('should return vehicle state at the given timestamp and cache if found for next request', async () => {
            const responses: any[] = [];

            for (let i = 1; i <= 2; i++) {
                responses.push(await vehicleService.getStateAt(2, '2022-09-10 14:59:01+00'));
            }

            expect(responses).toHaveLength(2);
            expect(responses[0]).toEqual({
                make: 'AUDI',
                model: 'A4',
                state: 'quoted',
                currentState: 'selling',
            });
            expect(responses[1]).toEqual({
                make: 'AUDI',
                model: 'A4',
                state: 'quoted',
                currentState: 'selling',
            });

            expect(vehicleRepoFindStateAtSpy.calledOnce).toBeTruthy();

            expect(cacheServiceSetSpy.callCount).toEqual(2);
            expect(cacheServiceSetSpy.args[0]).toEqual([
                'vehicle-state:2',
                JSON.stringify({
                    id: 2,
                    make: 'AUDI',
                    model: 'A4',
                    currentState: 'selling',
                    stateLogs: [],
                }),
            ]);
            expect(cacheServiceSetSpy.args[1]).toEqual([
                'vehicle-state:2:2022-09-10 14:59:01+00',
                JSON.stringify({
                    id: 2,
                    make: 'AUDI',
                    model: 'A4',
                    currentState: 'selling',
                    stateLogs: [{
                        vehicleId: 2,
                        state: 'quoted',
                        timestamp: '2022-09-08T14:59:01.000Z'
                    }],
                }),
            ]);
        });

        test.each([
            {
                description: 'future timestamp should return latest state',
                vehicleId: 3,
                timestamp: '2023-09-12 12:41:41+00',
                extraNote: 'latest in DB is 2022-09-12 12:41:41+00',
                expectedState: 'sold',
                currentState: 'sold',
            },
            {
                description: 'timestamp in between two states should return the state of the older timestamp (time difference)',
                vehicleId: 3,
                timestamp: '2022-09-12 12:40:41+00',
                extraNote: 'the given timestamp is in between 2022-09-11 23:21:38+00 and 2022-09-12 12:41:41+00 - the expected state should be for 2022-09-11 23:21:38+00',
                expectedState: 'selling',
                currentState: 'sold',
            },
            {
                description: 'timestamp in between two states should return the state of the older timestamp (date difference)',
                vehicleId: 2,
                timestamp: '2022-09-10 14:59:01+00',
                extraNote: 'the given timestamp is in between 2022-09-08 14:59:01+00 and 2022-09-11 17:03:17+00 - the expected state should be for 2022-09-11 17:03:17+00',
                expectedState: 'quoted',
                currentState: 'selling',
            },
            {
                description: 'timestamp matches exactly',
                vehicleId: 2,
                timestamp: '2022-09-11 17:03:17+00',
                extraNote: 'the given timestamp matches an actual recorded timestamp',
                expectedState: 'selling',
                currentState: 'selling',
            },
        ])('should return accurate result: $description', async({vehicleId, timestamp, expectedState, currentState}) => {
            const response = await vehicleService.getStateAt(vehicleId, timestamp);

            expect(response.state).toEqual(expectedState);
            expect(response.currentState).toEqual(currentState);
        });
    });

    describe('clearVehicleAsNotFound', () => {
        it('should clear vehicle from cache', async () => {
            await vehicleService.clearVehicleAsNotFound(2);

            expect(cacheServiceDeleteSpy.calledOnceWith('vehicle-state:2:not-found')).toBeTruthy();
        });
    });
});