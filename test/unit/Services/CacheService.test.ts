import { CacheService, RedisAdapter } from "../../../src/services/CacheService";
import { Container } from "typedi";
import sinon, { SinonSandbox } from "sinon";
import { RedisMock } from "../../utils/mocks/RedisMock";
import { bootstrap, BootstrapSetting } from "../../utils/bootstrap";
import { dropDatabaseConnection } from "../../utils/database";

describe('Cache Service', () => {
    let settings: BootstrapSetting;
    let sandbox: SinonSandbox;
    let cacheService: CacheService;
    let redisClient: RedisMock;
    let adapter: RedisAdapter;

    beforeAll(async () => {
        settings = await bootstrap();

        redisClient = new RedisMock();
        adapter = new RedisAdapter(redisClient as any);
        Container.set('redis-cache', adapter);
        cacheService = new CacheService();
    });

    afterAll(async () => {
        await dropDatabaseConnection(settings);
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe('get', () => {
        it('should execute actions on the chosen cache-provider: GET', async () => {
            const redisMockSpy = sandbox.spy(redisClient, 'get');
            const adapterSpy = sandbox.spy(adapter, 'get');

            await cacheService.get('key-2');

            expect(adapterSpy.calledOnce).toBeTruthy();
            expect(adapterSpy.args[0]).toEqual(['key-2']);
            expect(redisMockSpy.calledOnce).toBeTruthy();
            expect(redisMockSpy.args[0]).toEqual(['key-2']);
        });
    });

    describe('set', () => {
        it('should execute actions on the chosen cache-provider: SET with custom time', async () => {
            const redisMockSpy = sandbox.spy(redisClient, 'set');
            const adapterSpy = sandbox.spy(adapter, 'set');

            await cacheService.set('key-2', 'fake-value', 120);

            expect(adapterSpy.calledOnce).toBeTruthy();
            expect(adapterSpy.args[0]).toEqual([
                'key-2',
                'fake-value',
                120,
            ]);
            expect(redisMockSpy.calledOnce).toBeTruthy();
            expect(redisMockSpy.args[0]).toEqual([
                'key-2',
                'fake-value',
                {
                    EX: 120,
                },
            ]);
        });

        it('should execute actions on the chosen cache-provider: SET with default time', async () => {
            const redisMockSpy = sandbox.spy(redisClient, 'set');
            const adapterSpy = sandbox.spy(adapter, 'set');

            await cacheService.set('key-2', 'fake-value');

            expect(adapterSpy.calledOnce).toBeTruthy();
            expect(adapterSpy.args[0]).toEqual([
                'key-2',
                'fake-value',
                60,
            ]);
            expect(redisMockSpy.calledOnce).toBeTruthy();
            expect(redisMockSpy.args[0]).toEqual([
                'key-2',
                'fake-value',
                {
                    EX: 60,
                },
            ]);
        });
    });

    describe('delete', () => {
        it('should execute actions on the chosen cache-provider: DELETE', async () => {
            const redisMockSpy = sandbox.spy(redisClient, 'del');
            const adapterSpy = sandbox.spy(adapter, 'delete');

            await cacheService.delete('key-2');

            expect(adapterSpy.calledOnce).toBeTruthy();
            expect(adapterSpy.args[0]).toEqual(['key-2']);
            expect(redisMockSpy.calledOnce).toBeTruthy();
            expect(redisMockSpy.args[0]).toEqual(['key-2']);
        });
    });
});