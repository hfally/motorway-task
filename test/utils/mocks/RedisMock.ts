import { Service } from "typedi";

@Service()
export class RedisMock {
    private memory: Record<string, string> = {};

    public async set(key: any, value: any) {
        this.memory[key] = value;

        // note: no actual expiry will be set in test because of setTimeout leaks.
    }

    public async get(key: string) {
        return Promise.resolve(this.memory[key]);
    }

    public async del(key: string) {
        delete this.memory[key];
    }


    public disconnect() {
        this.refreshMemory();
    }

    public connect() {
        this.refreshMemory();
    }

    /**
     * Reset the memory.
     */
    public refreshMemory() {
        this.memory = {};
    }
}