import { Service } from "typedi";

@Service()
export class LoggerMock {
    public error(errors: any[]) {
        console.error(...errors);
    }
}
