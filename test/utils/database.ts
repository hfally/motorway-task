import { BootstrapSetting } from "./bootstrap";
import { Container } from "typedi";
import { DataSource, EntityTarget, Repository } from "typeorm";

export async function dropDatabaseConnection(app: BootstrapSetting) {
    await app.connection.destroy();
}

/**
 * Get typeorm raw repository for an entity.
 */
export function getRawRepo(entity: EntityTarget<unknown>): Repository<any> {
    const datasource = Container.get('defaultConnection') as DataSource;
    return datasource.getRepository(entity as any);
}

/**
 * Truncate a table for the given entity.
 */
export async function truncateTable(entity: EntityTarget<unknown>): Promise<void> {
    const repo = getRawRepo(entity);
    await repo.query(`TRUNCATE TABLE "${repo.metadata.tableName}" RESTART IDENTITY CASCADE`);
}