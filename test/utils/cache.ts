import { Container } from "typedi";
import { RedisMock } from "./mocks/RedisMock";

export function refreshCacheMemory() {
    const client = Container.get(RedisMock);
    client.refreshMemory();
}
