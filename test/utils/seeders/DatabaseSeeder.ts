import { StateLogSeeder } from "./StateLogSeeder";
import { VehicleSeeder } from "./VehicleSeeder";

export class DatabaseSeeder {
    public static async seed() {
        // register new seeder here.
        const seeders = [
            VehicleSeeder,
            StateLogSeeder,
        ];

        for (const seeder of seeders) {
            await (new seeder).seed();
        }
    }
}