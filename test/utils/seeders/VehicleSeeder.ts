import { Container } from "typedi";
import { DataSource } from "typeorm";
import { SeederInterface } from "./SeederInterface";
import { Vehicle } from "../../../src/models/Vehicle";
import { truncateTable } from "../database";

export class VehicleSeeder implements SeederInterface{
    public async seed() {
        const datasource = Container.get('defaultConnection') as DataSource;
        const repo = datasource.getRepository(Vehicle);
        const records = [
            {
                id: 1,
                make: 'BMW',
                model: 'X1',
                currentState: 'quoted',
            },
            {
                id: 2,
                make: 'AUDI',
                model: 'A4',
                currentState: 'selling',
            },
            {
                id: 3,
                make: 'VW',
                model: 'GOLF',
                currentState: 'sold',
            },
        ] as any[];

        await truncateTable(Vehicle);
        await repo.insert(records);
    }
}
