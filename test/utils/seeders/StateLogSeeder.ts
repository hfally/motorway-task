import { Container } from "typedi";
import { DataSource } from "typeorm";
import { SeederInterface } from "./SeederInterface";
import { StateLog } from "../../../src/models/StateLog";
import { truncateTable } from "../database";

export class StateLogSeeder implements SeederInterface{
    public async seed() {
        const datasource = Container.get('defaultConnection') as DataSource;
        const repo = datasource.getRepository(StateLog);
        const records = [
            {
                vehicleId: 1,
                state: 'quoted',
                timestamp: '2022-09-10 10:23:54+00'
            },
            {
                vehicleId: 2,
                state: 'quoted',
                timestamp: '2022-09-08 14:59:01+00'
            },
            {
                vehicleId: 2,
                state: 'selling',
                timestamp: '2022-09-11 17:03:17+00'
            },
            {
                vehicleId: 3,
                state: 'quoted',
                timestamp: '2022-09-11 09:11:45+00'
            },
            {
                vehicleId: 3,
                state: 'selling',
                timestamp: '2022-09-11 23:21:38+00'
            },
            {
                vehicleId: 3,
                state: 'sold',
                timestamp: '2022-09-12 12:41:41+00'
            },
        ] as any[];

        await truncateTable(StateLog);
        await repo.insert(records);
    }
}
