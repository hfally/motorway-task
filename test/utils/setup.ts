import 'reflect-metadata';
import { DataSource } from "typeorm";
import { env } from "../../src/env";

async function refreshDatabase() {
    const database = 'motorway_test_db';

    const datasource = new DataSource({
        type: 'postgres',
        host: env.database.host,
        port: env.database.port,
        username: env.database.username,
        password: env.database.password,
        synchronize: true,
        logging: true,
        database: 'postgres',
    });

    await datasource.initialize();

    await datasource.query(`DROP DATABASE IF EXISTS "${database}"`);
    await datasource.query(`CREATE DATABASE "${database}"`);
    await datasource.destroy();
}

module.exports = async () => {
    await refreshDatabase();
}
