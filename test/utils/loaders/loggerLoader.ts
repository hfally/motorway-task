import { MicroframeworkLoader } from "microframework-w3tec";
import { Container } from "typedi";
import { LoggerMock } from "../mocks/LoggerMock";

export const loggerLoader: MicroframeworkLoader = async () => {
    const logger = Container.get(LoggerMock);

    Container.set('logger', logger);
};
