import { Container } from "typedi";
import { MicroframeworkLoader } from "microframework-w3tec";
import { RedisMock } from "../mocks/RedisMock";

export const cacheLoader: MicroframeworkLoader = async (settings) => {
    const client = Container.get(RedisMock);

    client.connect();

    Container.set('redis-client', client);
    Container.set('cache-provider', 'redis-cache');

    if (settings) {
        settings.onShutdown(() => client.disconnect());
    }
};
