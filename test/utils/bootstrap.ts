import { bootstrapMicroframework } from "microframework-w3tec";
import { expressLoader, iocContainer } from "../../src/loaders";
import {
    cacheLoader,
    loggerLoader,
    typeormLoader,
} from "./loaders";
import { Application } from "express";
import * as http from "http";
import { DataSource } from "typeorm";

export type BootstrapSetting = {
    app: Application,
    server: http.Server,
    connection: DataSource,
}

export const bootstrap = async (): Promise<BootstrapSetting> => {
    const framework = await bootstrapMicroframework({
        loaders: [
            typeormLoader,
            iocContainer,
            loggerLoader,
            expressLoader,
            cacheLoader
        ],
    });

    return {
        app: framework.settings.getData('express_app') as Application,
        server: framework.settings.getData('express_server') as http.Server,
        connection: framework.settings.getData('defaultConnection') as DataSource,
    }
}
