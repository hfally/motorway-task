import { VehicleService } from "../services/VehicleService";
import { Get, JsonController, Param, QueryParams } from "routing-controllers";
import { OpenAPI, ResponseSchema } from "routing-controllers-openapi";
import { Service } from "typedi";
import { GetStateRequest } from "../validations/schemas/GetStateRequest";
import { GetStateResponse } from "../dtos/GetStateResponse";
import { ErrorResponse } from "../dtos/ErrorResponse";

@Service()
@JsonController('/vehicles')
export class VehicleController {
    constructor(private vehicleService: VehicleService) {}

    @Get('/:vehicleId/state')
    @OpenAPI({
        summary: 'Fetch Vehicle State',
        description: 'Get the state of the vehicle at the given timestamp.',
    })
    @ResponseSchema(GetStateResponse as any, { statusCode: 200 })
    @ResponseSchema(ErrorResponse as any, { statusCode: 400 })
    public async getState(
        @Param('vehicleId') vehicleId: number,
        @QueryParams() { timestamp }: GetStateRequest
    ): Promise<GetStateResponse> {
        return {
            message: 'Vehicle state retrieved',
            data: await this.vehicleService.getState(vehicleId, timestamp)
        };
    }
}
