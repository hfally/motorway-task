import { glob } from "glob";
import { env } from "../env";
import { getFromContainer, MetadataStorage } from "class-validator";
import { validationMetadatasToSchemas } from "class-validator-jsonschema";
import { getMetadataArgsStorage } from "routing-controllers";
import { routingControllersToSpec } from "routing-controllers-openapi";
import path from "path";
import * as fs from "fs";
import deepEqual from 'deep-equal';

export function buildSwagger(): void {
    const routingControllersOption = {
        controllers: env.app.directories.controllers,
        middlewares: env.app.directories.middlewares,
        validation: true,
    };

    // dynamically load the controllers and then generate the swagger docs.
    routingControllersOption.controllers.forEach((path) => {
        const controllers = glob.sync(path);

        controllers.forEach((controller) => {
            require(controller);
        });
    });

    // parse routing-controllers classes into OpenAPI spec.
    const metadata = (getFromContainer(MetadataStorage) as any).validationMetadatas;
    const schemas = validationMetadatasToSchemas({
        ...metadata,
        refPointerPrefix: '#/components/schemas/',
    }) as any;
    const storage = getMetadataArgsStorage();
    const spec = routingControllersToSpec(storage, routingControllersOption, {
        components: { schemas },
        info: {
            title: env.app.name,
            description: env.app.description,
            version: env.app.version,
        },
    });

    const swaggerFile = path.join(__dirname, '../', env.swagger.file);
    let content;

    try {
        fs.statSync(swaggerFile);
    } catch (error) {
        content = require(swaggerFile);
    }

    const newSpec = JSON.stringify(spec, null, '\t');

    if (!deepEqual(content, spec)) {
        try {
            fs.writeFileSync(swaggerFile, newSpec, 'utf-8');
            console.log('Done generating swagger docs.');
        } catch (error) {
            console.error('Error generating swagger docs:', error)
        }
    }
}