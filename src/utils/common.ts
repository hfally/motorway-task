import { join } from 'path';

/**
 * Check if environment is test.
 */
export function isTest(): boolean {
    return process.env.NODE_ENV === 'test';
}

/**
 * Check if environment is development.
 */
export function isDevelopment(): boolean {
    return process.env.NODE_ENV === 'development';
}

/**
 * Check if environment is production.
 */
export function isProduction(): boolean {
    return process.env.NODE_ENV === 'production';
}

/**
 * Get environment variable.
 */
export function getEnv(name: string, defaultValue?: unknown): unknown {
    return (name in process.env) ? process.env[name] : defaultValue;
}

/**
 * Get all the full paths.
 */
export function getPaths(paths: string[]) {
    return paths.map((path) => getPath(path));
}

/**
 * Get the full path.
 */
export function getPath(path: string) {
    return isProduction()
        ? join(process.cwd(), path.replace('src/', 'dist/src/').slice(0, -3) + '.js')
        : join(process.cwd(), path);
}

/**
 * Convert passed value to boolean.
 */
export function toBool(value: any) {
    const truthy = [
        '1',
        'TRUE',
    ];

    return truthy.includes(String(value).toUpperCase());
}
