import { Service } from "typedi";
import { VehicleRepository } from "../repositories/VehicleRepository";
import { CacheService } from "./CacheService";
import { BadRequestError, NotFoundError } from "routing-controllers";
import { ErrorMessage } from "../enums/ErrorMessage";
import { Vehicle } from "../models/Vehicle";
import { LogService } from "./LogService";
import { StateLog } from "../models/StateLog";
import { VehicleState } from "../dtos/VehicleState";

@Service()
export class VehicleService {
    constructor(
        private vehicleRepo: VehicleRepository,
        private cacheService: CacheService,
        private logger: LogService,
    ) {}

    /**
     * Retrieve the state of a vehicle.
     */
    public async getState(id: number, timestamp?: string): Promise<VehicleState> {
        if (!timestamp) {
            return this.getCurrentState(id);
        }

        return this.getStateAt(id, timestamp);
    }

    /**
     * Get the current state of a vehicle.
     */
    public async getCurrentState(id: number): Promise<VehicleState> {
        const cacheKey = VehicleService.cacheKey(id);
        // check if id has been stored as not-found before.
        const vehicleNotFound = await this.cacheService.get(`${cacheKey}:not-found`);

        if (vehicleNotFound) {
            throw new NotFoundError(ErrorMessage.VEHICLE_NOT_FOUND);
        }

        // check if vehicle record is in cache.
        const cachedState = await this.cacheService.get(cacheKey);

        if (cachedState) {
            const record: Vehicle = JSON.parse(cachedState);

            return {
                make: record.make,
                model: record.model,
                state: record.currentState,
                currentState: record.currentState,
            };
        }

        // fetch from DB.
        const record = await this.vehicleRepo.find(id);

        if (!record) {
            this.cacheVehicleAsNotFound(id);
            throw new NotFoundError(ErrorMessage.VEHICLE_NOT_FOUND);
        }

        // store in cache.
        this.cacheService.set(cacheKey, JSON.stringify(record)).catch((error) => {
            this.logger.error(error);
        });

        return {
            make: record.make,
            model: record.model,
            state: record.currentState,
            currentState: record.currentState,
        }
    }

    /**
     * Get the state of a vehicle at a given timestamp.
     */
    public async getStateAt(id: number, timestamp: string): Promise<VehicleState> {
        const vehicleCacheKey = VehicleService.cacheKey(id);
        const timestampCacheKey = `${vehicleCacheKey}:${timestamp}`;
        // check if id has been stored as not-found before.
        const vehicleNotFound = await this.cacheService.get(`${vehicleCacheKey}:not-found`);

        if (vehicleNotFound) {
            throw new NotFoundError(ErrorMessage.VEHICLE_NOT_FOUND);
        }

        const timestampNotFound = await this.cacheService.get(`${timestampCacheKey}:not-found`);

        if (timestampNotFound) {
            throw new BadRequestError(ErrorMessage.TIMESTAMP_NOT_FOUND);
        }

        const cachedTimestampResult = await this.cacheService.get(timestampCacheKey);

        if (cachedTimestampResult) {
            const vehicle: Vehicle = JSON.parse(cachedTimestampResult);
            const stateLog: StateLog = vehicle.stateLogs[0];

            return {
                make: vehicle.make,
                model: vehicle.model,
                currentState: vehicle.currentState,
                state: stateLog.state,
            };
        }

        const vehicle = await this.vehicleRepo.findStateAt(id, timestamp);

        if (!vehicle) {
            this.cacheVehicleAsNotFound(id);
            throw new NotFoundError(ErrorMessage.VEHICLE_NOT_FOUND);
        }

        const stateLog: StateLog = vehicle.stateLogs[0];

        if (!stateLog) {
            this.cacheService.set(`${timestampCacheKey}:not-found`, 'true').catch((error) => {
                this.logger.error(error);
            });
            throw new BadRequestError(ErrorMessage.TIMESTAMP_NOT_FOUND);
        }

        // cache vehicle only.
        this.cacheService.set(vehicleCacheKey, JSON.stringify({
            ...vehicle,
            stateLogs: [],
        })).catch((error) => {
            this.logger.error(error);
        });

        // cache timestamp result.
        this.cacheService.set(timestampCacheKey, JSON.stringify(vehicle))
            .catch((error) => {
                this.logger.error(error);
            });

        return {
            make: vehicle.make,
            model: vehicle.model,
            currentState: vehicle.currentState,
            state: stateLog.state,
        }
    }

    /**
     * Store vehicle as not found.
     */
    private cacheVehicleAsNotFound(id: number) {
        const cacheKey = VehicleService.cacheKey(id);
        const dayInSeconds = 60 * 60 * 24;
        this.cacheService.set(`${cacheKey}:not-found`, 'true', dayInSeconds).catch((error) => {
            this.logger.error(error);
        });
    }

    /**
     * Clear vehicle as not found.
     */
    public async clearVehicleAsNotFound(id: number) {
        const cacheKey = VehicleService.cacheKey(id);
        await this.cacheService.delete(`${cacheKey}:not-found`);
    }

    /**
     * Construct main cache key for a vehicle.
     */
    private static cacheKey(id: number) {
        return `vehicle-state:${id}`
    }
}
