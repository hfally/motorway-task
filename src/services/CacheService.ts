import { env } from "../env";

export * from './cache-providers';
import { CacheProviderInterface } from "./cache-providers/CacheProviderInterface";
import { Container, Service } from "typedi";

@Service()
export class CacheService{
    private readonly cacheProvider: CacheProviderInterface;
    
    constructor() {
        const providerName = Container.get('cache-provider') as string;
        this.cacheProvider = Container.get(providerName);
    }

    public async get(key: string): Promise<string|null> {
        return this.cacheProvider.get(key);
    }

    public async set(key: string, value: string, expiryInSeconds = env.cache.expiry): Promise<boolean> {
        return this.cacheProvider.set(key, value, expiryInSeconds);
    }

    public async delete(key: string): Promise<boolean> {
        return this.cacheProvider.delete(key);
    }
}
