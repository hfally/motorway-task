export interface CacheProviderInterface {
    /**
     * Save value in cache-providers.
     */
    set(key: string, value: unknown, expiry: number): Promise<boolean>;

    /**
     * Retrieve data from cache-providers.
     */
    get(key: string): Promise<string | null>|null;

    /**
     * Delete data from cache-providers.
     */
    delete(key: string): Promise<boolean>;
}
