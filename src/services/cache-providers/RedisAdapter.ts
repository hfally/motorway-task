import { CacheProviderInterface } from "./CacheProviderInterface";
import { Inject, Service } from "typedi";
import { RedisClientType } from "redis";

@Service('redis-cache')
export class RedisAdapter implements CacheProviderInterface {
    constructor(@Inject('redis-client') private redis: RedisClientType) {}

    public async get(key: string): Promise<string | null> {
        return this.redis.get(key as any);
    }

    public async set(key: string, value: unknown, expiry: number): Promise<boolean> {
        await this.redis.set(
            key as any,
            value as any,
            {
                EX: expiry,
            } as any
        );

        return true;
    }

    public async delete(key: string): Promise<boolean> {
        await this.redis.del(key as any);

        return true;
    }
}