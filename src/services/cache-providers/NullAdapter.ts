import { CacheProviderInterface } from "./CacheProviderInterface";
import { Service } from "typedi";

@Service('null-cache')
export class NullAdapter implements CacheProviderInterface {
    public get(): null {
        return null;
    }

    public async set(): Promise<boolean> {
        return Promise.resolve(true);
    }

    public async delete(): Promise<boolean> {
        return Promise.resolve(true);
    }
}