import { Inject, Service } from "typedi";
import {Logger} from "winston";

@Service()
export class LogService {
    constructor(@Inject('logger') private logger: Logger) {}

    /**
     * Log an error.
     */
    public error(...errors: any[]) {
        this.logger.error(errors);
    }
}
