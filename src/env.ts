import path from 'path';
import dotenv from 'dotenv';
import {
    getEnv,
    getPaths,
    isDevelopment,
    isProduction,
    isTest,
    toBool,
} from "./utils/common";
import pkg from '../package.json';

/**
 * Load .env or .env.test for tests.
 */
dotenv.config({
    path: path.join(process.cwd(), `.env${ isTest() ? '.test' : '' }`),
});

export const env = {
    environment: getEnv('NODE_ENV', 'development'),
    isTest: isTest(),
    isDevelopment: isDevelopment(),
    isProduction: isProduction(),

    app: {
        name: pkg.name,
        version: pkg.version,
        description: pkg.description,
        url: getEnv('APP_URL') as string,
        port: getEnv('APP_PORT') as number,
        debug: toBool(getEnv('APP_DEBUG', false)),

        directories: {
            entities: getPaths(
                String(getEnv('TYPEORM_ENTITIES', 'src/models/*.ts')).split(',')
            ),
            controllers: getPaths(
                String(getEnv('CONTROLLERS', 'src/controllers/**/*.ts')).split(',')
            ),
            middlewares: getPaths(
                String(getEnv('MIDDLEWARES', 'src/middlewares/**/*.ts')).split(',')
            ),
        },
    },

    database: {
        type: getEnv('TYPEORM_CONNECTION', 'postgres') as any,
        host: getEnv('TYPEORM_HOST') as string,
        port: getEnv('TYPEORM_PORT', 5432) as number,
        username: getEnv('TYPEORM_USERNAME') as string,
        password: getEnv('TYPEORM_PASSWORD') as string,
        database: getEnv('TYPEORM_DATABASE') as string,
        synchronize: toBool(getEnv('TYPEORM_SYNCHRONIZE', false)),
        logging: toBool(getEnv('TYPEORM_LOGGING', false)),
    },

    cache: {
        connection: getEnv('CACHE_CONNECTION', 'redis') as any,
        host: getEnv('CACHE_HOST', '127.0.0.1') as string,
        port: getEnv('CACHE_PORT', 6379) as number,
        password: getEnv('CACHE_PASSWORD') as string,
        expiry: getEnv('CACHE_EXPIRY', 60) as number,
    },

    swagger: {
        enabled: toBool(getEnv('SWAGGER_ENABLED', true)),
        route: getEnv('SWAGGER_ROUTE', '/swagger') as string,
        file: getEnv('SWAGGER_FILE', 'swagger.json') as string,
    },
};
