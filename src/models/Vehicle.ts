import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { State } from "../enums/State";
import { StateLog } from "./StateLog";

@Entity('vehicles')
export class Vehicle {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column({type: 'text'})
    public make: string;

    @Column({type: 'text'})
    public model: string;

    @Column({ type: 'text', name: 'state' })
    public currentState: State;

    @OneToMany(() => StateLog, (stateLog) => stateLog.vehicle)
    public stateLogs: StateLog[];
}
