import { Column, Entity, ManyToOne, PrimaryColumn } from "typeorm";
import { State } from "../enums/State";
import { Vehicle } from "./Vehicle";

@Entity('stateLogs')
export class StateLog {
    @PrimaryColumn({ type: 'integer' })
    public vehicleId: number;

    @Column({ type: 'text' })
    public state: State;

    @PrimaryColumn({ type: 'timestamptz' })
    public timestamp: string;

    @ManyToOne(() => Vehicle, (vehicle) => vehicle.stateLogs)
    public vehicle: Vehicle;
}
