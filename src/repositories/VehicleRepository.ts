import { Inject, Service } from "typedi";
import { DataSource, Repository } from "typeorm";
import { Vehicle } from "../models/Vehicle";

@Service()
export class VehicleRepository {
    private repo: Repository<Vehicle>;

    constructor(
        @Inject('defaultConnection') datasource: DataSource
    ) {
        this.repo = datasource.getRepository(Vehicle);
    }

    /**
     * Retrieve a vehicle from storage.
     */
    public async find(id: number): Promise<Vehicle | null> {
        return this.repo.findOne({
            where: {
                id,
            },
        });
    }

    /**
     * Fetch vehicle and its state at a timestamp.
     */
    public async findStateAt(id: number, timestamp: string): Promise<Vehicle | null> {
        return this.repo.createQueryBuilder('vehicle')
            .leftJoinAndSelect(
                'vehicle.stateLogs',
                'stateLog',
                'stateLog.timestamp <= :timestamp',
                { timestamp }
            )
            .where('vehicle.id = :id', { id })
            .orderBy('stateLog.timestamp', 'DESC')
            .limit(1)
            .getOne();
    }

    /**
     * Create a vehicle.
     */
    public async create() {
        // NB: make sure to clear generated ID from not-found cache. -- this.vehicleService.clearVehicleAsNotFound(id)
    }
}
