import { createClient } from "redis";
import { Container } from "typedi";
import { MicroframeworkLoader } from "microframework-w3tec";
import { env } from "../env";

function connectToRedis() {
    const socket: any = {
        host: env.cache.host,
    };

    if (env.cache.port) {
        socket.port = env.cache.port;
    }

    if (env.cache.password) {
        socket.password = env.cache.password;
    }

    return createClient({ socket });
}

export const cacheLoader: MicroframeworkLoader = async (settings) => {
    try {
        switch (String(env.cache.connection).toUpperCase().trim()) {
            case 'REDIS': {
                const client = connectToRedis();
                await client.connect();

                Container.set('redis-client', client);
                Container.set('cache-provider', 'redis-cache');

                if (settings) {
                    settings.onShutdown(() => client.disconnect());
                }

                break;
            }
            case 'NULL': {
                Container.set('cache-provider', 'null-cache');
                break;
            }
            default:
                throw new Error(`Unknown cache connection driver: ${env.cache.connection}`);
        }
    } catch (error) {
        console.error('Error connecting to cache-provider:', error);
        throw error;
    }
};
