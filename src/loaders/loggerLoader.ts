import { MicroframeworkLoader } from "microframework-w3tec";
import { createLogger, transports } from "winston";
import { Container } from "typedi";

export const loggerLoader: MicroframeworkLoader = async () => {
    const logger = createLogger({
        transports: [
            new transports.Console(),
        ],
    });

    Container.set('logger', logger);
};
