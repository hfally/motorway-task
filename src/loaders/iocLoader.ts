import { MicroframeworkLoader } from "microframework-w3tec";
import { useContainer } from "routing-controllers";
import { Container } from "typedi";

export const iocContainer: MicroframeworkLoader = () => {
    useContainer(Container);
}
