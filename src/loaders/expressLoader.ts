import { Application } from "express";
import { MicroframeworkLoader } from "microframework-w3tec";
import { createExpressServer } from "routing-controllers";
import gracefulExit from 'express-graceful-exit';

import { env } from "../env";

export const expressLoader: MicroframeworkLoader = (settings) => {
    if (!settings) {
        return;
    }

    const app: Application = createExpressServer({
        cors: true,
        classTransformer: true,
        defaultErrorHandler: false,
        controllers: env.app.directories.controllers,
        middlewares: env.app.directories.middlewares,
    });

    if (!env.isTest) {
        const server = app.listen(env.app.port);

        settings.setData('express_server', server);
        gracefulExit.init(server);
        app.use(gracefulExit.middleware(app as any));

        settings.onShutdown(() => {
            gracefulExit.gracefulExitHandler(app as any, server, {
                log: true,
                performLastRequest: true,
                errorDuringExit: true,
                exitProcess: true,
                suicideTimeout: 60000,
                callback: () => {
                    console.log('Shutting down...');
                },
            });
        });
    }

    settings.setData('express_app', app);
}