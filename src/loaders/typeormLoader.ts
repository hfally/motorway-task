import { MicroframeworkLoader } from "microframework-w3tec";
import { DataSource } from "typeorm";
import { env } from "../env";
import { Container } from "typedi";

export const typeormLoader: MicroframeworkLoader = async (settings) => {
    try {
        const datasource = new DataSource({
            type: env.database.type,
            host: env.database.host,
            port: env.database.port,
            username: env.database.username,
            password: env.database.password,
            database: env.database.database,
            synchronize: env.database.synchronize,
            logging: env.database.logging,
            entities: env.app.directories.entities,
        });

        await datasource.initialize();

        Container.set('defaultConnection', datasource);

        if (settings) {
            settings.onShutdown(() => datasource.destroy());
        }
    } catch (error) {
        console.error('Error connecting to database: ', error);
        throw error;
    }
};
