import { MicroframeworkLoader } from "microframework-w3tec";
import { env } from "../env";
import path from "path";
import swaggerUi from 'swagger-ui-express';

export const swaggerLoader: MicroframeworkLoader = (settings) => {
    if (!settings || !env.swagger.enabled) {
        return;
    }

    const app = settings.getData('express_app');
    const swaggerFile = require(path.join(__dirname, '..', env.swagger.file));

    swaggerFile.info = {
        title: env.app.name,
        description: env.app.description,
        version: env.app.version,
    }

    swaggerFile.servers = [
        {
            url: '/',
        },
    ];

    app.use(
        env.swagger.route,
        swaggerUi.serve,
        swaggerUi.setup(swaggerFile)
    );
};
