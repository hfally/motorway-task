import 'reflect-metadata';
import { bootstrapMicroframework } from "microframework-w3tec";
import { env } from "./env";
import { buildSwagger } from "./utils/swagger";
import {
    cacheLoader,
    expressLoader,
    iocContainer,
    loggerLoader,
    swaggerLoader,
    typeormLoader,
} from "./loaders";

if (env.isDevelopment) {
    buildSwagger();
}

const loaders = [
    typeormLoader,
    iocContainer,
    loggerLoader,
    expressLoader,
    swaggerLoader,
    cacheLoader,
];

bootstrapMicroframework({ loaders }).then((framework) => {
    const signalTraps: NodeJS.Signals[] = [ 'SIGTERM', 'SIGINT', 'SIGUSR2' ];

    signalTraps.map((signal) => {
        process.once(signal, () => framework.shutdown());
    });
}).catch((error) => {
    console.error('Application failed to run: ', error);
});
