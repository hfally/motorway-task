import { IsString } from "class-validator";

export class VehicleState {
    @IsString()
    public make: string;

    @IsString()
    public model: string;

    @IsString()
    public state: string;

    @IsString()
    public currentState: string;
}
