import { IsObject, IsOptional, IsString } from "class-validator";

export class SuccessResponse<T> {
    @IsString()
    public message: string;

    @IsOptional()
    @IsObject()
    public data?: T;
}
