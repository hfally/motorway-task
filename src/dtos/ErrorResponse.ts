import { IsObject, IsOptional, IsString } from "class-validator";

export class ErrorResponse {
    @IsString()
    public message: string;

    @IsOptional()
    @IsObject()
    public errors?: Record<string, string[]>;

    public stack?: any;
    public requestBody?: any;
}
