import { SuccessResponse } from "./SuccessResponse";
import { VehicleState } from "./VehicleState";
import { ValidateNested } from "class-validator";
import { Type } from "class-transformer";

export class GetStateResponse extends SuccessResponse<VehicleState> {
    @ValidateNested()
    @Type(() => VehicleState)
    data: VehicleState;
}
