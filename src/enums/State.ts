export enum State {
    QUOTED = 'quoted',
    SELLING = 'selling',
    SOLD = 'sold',
}
