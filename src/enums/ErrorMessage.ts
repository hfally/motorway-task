export enum ErrorMessage {
    TIMESTAMP_NOT_FOUND = 'No state found for vehicle at the given timestamp',
    VEHICLE_NOT_FOUND = 'Vehicle not found',
}
