import { IsISO8601, IsOptional, Matches } from "class-validator";

export class GetStateRequest {
    @IsOptional()
    @IsISO8601({ strict: true })
    @Matches(new RegExp(/^\d{4}-\d{2}-\d{2}((\s|T)\d{2}:\d{2}(:\d{2})?(\+\d{2})?)?$/), {
        message: 'timestamp must match YYYY-MM-DD, YYYY-MM-DD HH:MM:SS or YYYY-MM-DD HH:MM:SS+OFFSET'
    })
    timestamp?: string;
}
