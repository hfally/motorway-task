import { BadRequestError, ExpressErrorMiddlewareInterface, Middleware, NotFoundError } from "routing-controllers";
import { Request, Response } from "express";
import { env } from "../env";
import { Service } from "typedi";
import { LogService } from "../services/LogService";
import { ErrorResponse } from "../dtos/ErrorResponse";

@Service()
@Middleware({ type: "after" })
export class ErrorHandler  implements ExpressErrorMiddlewareInterface {
    constructor(private logger: LogService) {}

    error(error: any, request: Request, response: Response, next: (err?: any) => any): void {
        let body: ErrorResponse;
        let statusCode: number;

        if (error instanceof BadRequestError) {
            const { message, errors } = error as any;
            statusCode = 400;
            body = {
                message,
            };

            if (errors && Array.isArray(errors)) {
                body.errors = ErrorHandler.standardizeErrors(errors);
            }
        } else if (error instanceof NotFoundError) {
            statusCode = 404;
            body = {
                message: error.message,
            };
        } else {
            statusCode = 500;

            body = {
                message: !env.isProduction ? error.message : 'An internal error occurred',
            };

            if (!env.isProduction && env.app.debug && error.stack) {
                body.stack = error.stack;
                body.requestBody = request.body;
            }

            this.logger.error(error);
        }

        response.status(statusCode).json(body);
        next();
    }

    private static standardizeErrors(errors: any[]) {
        const allErrors: any = {};

        for (const errorInstance of errors) {
            allErrors[errorInstance.property] = [];

            for (const constraint in errorInstance.constraints) {
                allErrors[errorInstance.property].push(errorInstance.constraints[constraint]);
            }
        }

        return allErrors;
    }
}
