#!/bin/sh

if [ -f 'docker.env.override' ]; then
  cp docker.env.override docker.env
fi
