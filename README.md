# Table of Content

- [Introduction](#introduction)
- [Setup](#setup)
    - [Local Setup](#local-setup)
    - [Docker Setup](#docker-setup)
    - [Production Setup](#production-setup)
- [Environment Variables](#environment-variables)
- [Testing](#testing)
- [Endpoints](#endpoints)

# Introduction

This is my solution
to [this task](https://motorway.notion.site/Senior-Backend-Engineer-Tech-Challenge-6e59f0edc5d942b0a591a2b1aa248b3f)
by [Motorway](https://motorway.co.uk/about). The task is to create an endpoint that returns the state of a vehicle and
other details of the vehicle at a given timestamp.

## Technologies

### Language

- Typescript

### Libraries

- Express
- Postgres
- Redis
- TypeORM
- routing-controller
- Class-Validator
- Microframework-w3tec
- Swagger OpenAPI Spec
- Winston - Logger

# Setup

This application can be run both locally on a machine that has the requirements and in a docker container.

## Local Setup

To run this application locally, these must be installed on the machine:

- Node - >= 20
- Postgres (optional)
- Redis (optional)

Postgres and Redis are marked as optional because you can spin up docker containers for these two from within this
application. To spin them up using docker, head into the [database](/database) folder from the application's root
directory.

```shell
$ cd database
```

Run docker composer to spin up postgres and redis.

```shell
$ docker compose up -d
```

In order to do this, you must have docker installed on your machine.

**NB: You do not need to spin up these containers if you already have a postgres and redis connection you want to use.**

Once you are certain your machine has met the requirements, next is to make a copy of the example env file in the root
directory and edit to suit your environment (see [environment variables](#environment-variables)).

```shell
$ cp .env.example .env
```

Install all dependencies.

```shell
$ npm install
```

Run the application.

```shell
$ npm run serve
```

## Docker Setup

You can also spin up this application in a docker container without the hassle of trying to make your machine fit the
requirements for a local setup.

In order to do this, make sure you already have [docker](https://docs.docker.com/) installed on your machine. Then,
simply run this command in your terminal from the root directory of this application.

```shell
$ docker compose up -d
```

This will spin up these three things for you automatically:

- Postgres DB with sample data in it.
- Redis
- The application itself

Please note, unlike the local setup, the environment variables of the docker applications are stored in
the [docker.env](/docker.env). If you need to override this environment variables, simply create a `docker.env.override`
file.

```shell
$ cp docker.env docker.env.override
```

Edit the override file to your satisfaction and rebuild.

```shell
$ docker compose up --build -d
```

## Production Setup

To set up this application in production, it is advised to use docker. Similar to the [docker setup](#docker-setup)
above, the production server must have [docker](https://docs.docker.com/) installed on it.

To set the live credentials, create a docker.env override and edit with the right values.

```shell
$ cp docker.env docker.env.override
```

Once this is done, you can build and run the application in a container by running:

```shell
$ docker compose up --build --no-deps -d motorway-api-service
```

**_NB: the `--no-deps` flag means the `postgresdb` and `redisdb` docker containers meant for development will not be
built._**

# Environment Variables

| Variable            | Default                     | Allowed Values                       | Description                                                                                                                                                                                              |
|---------------------|-----------------------------|--------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| NODE_ENV            | development                 | test / development / production      | The environment the application is currently being run on.                                                                                                                                               |
| APP_URL             |                             | any url string including 'localhost' | The base url of this application.                                                                                                                                                                        |
| APP_PORT            |                             | any available port                   | The port node will serve this application on                                                                                                                                                             |
| APP_DEBUG           | false                       | false / true                         | Toggle if you want to see full stack in case of a 500. It should be set to false in production.                                                                                                          |
| CONTROLLERS         | src/controllers/\*\*/\*.ts  |                                      | Directory patterns to where controllers of the app are stored. Comma separated for multiple directories.                                                                                                 |
| MIDDLEWARES         | src/middlewares/\*\*\/\*.ts |                                      | Directory patterns to where middlewares of the app are stored. Comma separated for multiple directories.                                                                                                 |
| TYPEORM_CONNECTION  | postgres                    | postgres / mysql                     | The type of database driver to be used. Currently fully supports only postgres.                                                                                                                          |
| TYPEORM_HOST        |                             |                                      | Host to your postgres database.                                                                                                                                                                          |
| TYPEORM_PORT        | 5432                        |                                      | Port to your postgres connection.                                                                                                                                                                        |
| TYPEORM_USERNAME    |                             |                                      | Database connection username.                                                                                                                                                                            |
| TYPEORM_PASSWORD    |                             |                                      | Database connection password                                                                                                                                                                             |
| TYPEORM_DATABASE    |                             |                                      | Database name.                                                                                                                                                                                           |
| TYPEORM_SYNCHRONIZE | false                       | false / true                         | Toggle if you want typeorm to alter table structures to fit model definition. This should be false in production.                                                                                        |
| TYPEORM_LOGGING     | false                       | false / true                         | Toggle if you want to see the queries that typeorm is running. Best for debugging and should be false in production.                                                                                     |
| TYPEORM_ENTITIES    | src/models/*.ts             |                                      | Director pattern to where typeorm entities are stored.                                                                                                                                                   |
| CACHE_CONNECTION    | redis                       | redis / null                         | When set to redis, cache will be stored in redis. If set to null, the null-cache will be used which means nothing will really be cached. If set to any other thing, the application will throw an error. |
| CACHE_HOST          | 127.0.0.1                   |                                      | Host to redis connection.                                                                                                                                                                                |
| CACHE_PORT          | 6379                        |                                      | Port to your redis connection.                                                                                                                                                                           |
| CACHE_PASSWORD      |                             |                                      | Password, if any, for your redis connection.                                                                                                                                                             |
| CACHE_EXPIRY        | 60                          |                                      | Default cache expiry in seconds.                                                                                                                                                                         |
| SWAGGER_ENABLED     | true                        | true / false                         | Toggle if to setup swagger route.                                                                                                                                                                        |
| SWAGGER_ROUTE       | /swagger                    |                                      | The route the swagger docs will be available on.                                                                                                                                                         |
| SWAGGER_FILE        | swagger.json                |                                      | The file the OpenAPI spec generated content will be stored in for swagger.                                                                                                                               |

### The Null Cache

In case you want to run the application without an actual cache, you can set the `CACHE_CONNECTION` to string `null`.
This will let the application know to use the `NullCacheAdapter` - a process that allows the system to run exactly how
it would have if an actual cache connection like `redis` was up, but actually acts void.

# Testing

To run the test suites of this application, you'll need to have the [local setup](#local-setup) in place (node and
postgres only). However, in the case of testing you will need to make a copy of env as .env.test.

```shell
$ cp .env.example .env.test
```

The only thing you will need to edit is the database connection. As much as in-memory database would have been best for
unit testing, unfortunately, there isn't a great postgres alternative. However, the test suite will not use the database
name set in the `.env.test` to avoid mistakes. The test suite will automatically create a database
called `motorway_test_db`. This database will be automatically deleted when the tests are done running.

To run the test, run:

```shell
$ npm run test
```

# Endpoints

These are the endpoints in this application:

- **GET** /vehicles/:vehicleId/state?timestamp

**200 Success**

```json
{
  "message": "Vehicle state retrieved",
  "data": {
    "make": "BMW",
    "model": "X1",
    "state": "quoted",
    "currentState": "quoted"
  }
}
```

**400 Error: Bad Request**

```json
{
  "message": "Invalid queries, check 'errors' property for more info.",
  "errors": {
    "timestamp": [
      "timestamp must be a valid ISO 8601 date string"
    ]
  }
}
```

**404 Error: Not Found**

```json
{
  "message": "Vehicle not found"
}
```

You can test these endpoints yourself on swagger through the swagger route `<application-host>/swagger`. It is available
online on [http://motorway-task.tofunmifalade.com/swagger/](http://motorway-task.tofunmifalade.com/swagger/).