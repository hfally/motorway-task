FROM node:20.0.0-alpine3.16 as base

ENV APP_DIR /app/motorway-api

WORKDIR $APP_DIR

COPY . .

RUN ./override-docker-env.sh
RUN npm install
RUN npm run build

FROM base as motorway-api

COPY --from=base $APP_DIR/node_modules $APP_DIR/
COPY --from=base $APP_DIR/dist $APP_DIR/
COPY --from=base $APP_DIR/package*.json $APP_DIR/
COPY --from=base $APP_DIR/docker.env $APP_DIR/.env

WORKDIR $APP_DIR

CMD ["npm", "run", "start"]
